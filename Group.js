class Group {
  // Your code here.
  constructor() {
    this.group = [];
  }

  add(item) {
    if (!this.has(item)) {
      this.group.push(item);
    }
  }

  delete(item) {
    let index = this.group.indexOf(item);
    if (index >= 0) {
      this.group.splice(index, 1);
    }
  }

  deleteSlice(item) {
    let index = this.group.indexOf(item);
    let first;
    if (index >= 0) {
     first =  this.group.slice(0,index);
     this.group = (index > this.group.length -1)
       ?first.concat(this.group.slice(index+1))
     : first;
    }
  }

  has(item) {
    return this.group.includes(item);
  }


// does not eliminate duplicates
  // static from(iterable) {
  //   let object = new Group();
  //   object.group = [...iterable];
  //   return object;
  // }
// from solutions
  // static from(collection) {
  //   let group = new Group;
  //   for (let value of collection) {
  //     group.add(value);
  //   }
  //   return group;
  // }


  // delete(value) {
  //   this.members = this.members.filter(v => v !== value);
  // }

}

let group = Group.from([10, 20]);
console.log(group.has(10));
// → true
console.log(group.has(30));
// → false
group.add(10);
console.log(group)
group.delete(10);
console.log(group)
group.add(10);
console.log(group)
group.deleteSlice(10);
console.log(group)

console.log(group.has(10));
// → false

import {default as SuperClass,doSomething as justPrints} from './Address.js';
// this is the same
//import SuperClass from './Address.js';
class HomeAddress extends SuperClass {
    constructor(recipient, street, houseNr, postalCode, city, country) {
        super(recipient, postalCode, city, country);
        if (postalCode > MAX_ZIP) throw new Error ("zip is too high")
        this._street = street;
        this._houseNr = houseNr;
        justPrints()
    }

    toString() {
        return `${this.recipient}\n`
            + `${this._street} ${this._houseNr}\n`
            + super.toString() ;
    }
}

export {HomeAddress};

function executeConsumerTwice(someFunction, someArgument) {
    someFunction(someArgument);
    someFunction(someArgument);
}

function consumer(message) {
    console.log(message);
}

function count (max){
    for (let i=1 ;i<=max;i++){
        console.log(i)
    }
}

executeConsumerTwice(consumer, "Hello!");

// Let's use an existing function, one that we didn't write ourselves:
executeConsumerTwice(console.log, "Logging!");
executeConsumerTwice(count,5);
executeConsumerTwice(count, 2);

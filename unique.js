function unique(arr) {
  return arr.reduce((result,element)=> {
    if (!result.includes(element) ) result.push(element);
    return result;
  },
    [])
}

let strings = ["Hare", "Krishna", "Hare", "Krishna",
  "Krishna", "Krishna", "Hare", "Hare", ":-O"
];

console.log( unique(strings) ); // Hare, Krishna, :-O
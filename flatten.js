let arrays = [[1, 2, 3], [4, 5], [6]];
console.log(arrays.reduce((result, element) => result.concat(element)));
// without concat

// flatten without concat, using nested reduce (!)
console.log(arrays.reduce((result, element) =>
  element.reduce((subresult, subelement) => {
    subresult.push(subelement);
    return subresult
  }, result)
  , []));

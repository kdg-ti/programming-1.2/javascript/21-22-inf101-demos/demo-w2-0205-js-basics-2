
["a", "b", "c"].forEach(console.log);

console.log(["alfa", "beta", "gamma","delta","omicron"].filter(i => i.includes("a")));
console.log(["alfa", "beta", "gamma","delta","omicron"].find(i => i.includes("a")));

console.log(["alfa", "beta", "gamma","delta","omicron"]
  .filter(i => i.includes("a"))
  .map(i => i.toUpperCase())
  .reduce((result,val) => result + "-" + val)
)

console.log(["alfa", "beta", "gamma","delta","omicron"]
  .filter(i => i.includes("a"))
  .map(i => i.toUpperCase())
  .reduce((result,val) => result + val.length,0)
);


console.log(["alfa", "beta", "gamma","delta","omicron"]
  .filter(i => i.includes("a"))
  .map(i => i.toUpperCase())
  .sort((i,j) => (i<j)?1:-1)
)

console.log(["alfa", "beta", "gamma","delta","omicron"]
  .filter(i => i.includes("a"))
  .map(i => i.toUpperCase())
  .sort((i,j) => (i.length<j.length)?1:-1)
)